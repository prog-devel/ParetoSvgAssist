/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: svg_dataresolver.cpp
** Created: 2011-04-30 14:34:44.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#include "svg_dataresolver.h"
#include "svg_datamodel.h"

SvgDataResolver::SvgDataResolver(const QString &input, QScriptEngine *drScriptEngine)
    : m_input(input), m_drScriptEngine(drScriptEngine)
{

}

bool SvgDataResolver::resolve(DataModel *dataModel)
{
    clear();
    if(!dataModel || !isValid())
        return false;
    QRegExp rx(RESLV_ID_ATTR"\\s*=\\s*\"([^\"]*)([^>]*>)([^<]*)");
    int pos = rx.indexIn(m_input, 0);
    m_result += m_input.midRef(0, pos);

    QString warnStr;
    while(pos>=0)
    {
        QStringList capturedTexts = rx.capturedTexts();
        QString fieldName = capturedTexts[1];
        QString fieldText = capturedTexts[3];
        QVariant resolvedValue = formatData(fieldName, dataModel);
        QString resolvedText;

        if(!resolvedValue.isValid())
        {
            resolvedText = "!UNDEF!";
            warnStr += "Text field <b>\'" + fieldName + "\'</b> unresolved<br/>";
        }
        else
        {
            resolvedText = QString::fromUtf8(resolvedValue.toByteArray());
            qDebug() << "Resolving field" << fieldName << "=" << fieldText << "->" << resolvedText;
        }

        int capLen = capturedTexts[0].length();
        int nextPos = pos + capLen;
        int nLen = capLen - fieldText.length();
        m_result += m_input.midRef(pos, nLen);
        m_result += resolvedText;
        pos = rx.indexIn(m_input, nextPos);
        m_result += m_input.midRef(nextPos, pos - nextPos);
    }

    if(!warnStr.isEmpty())
        qWarning() << warnStr.toLocal8Bit().data();
//    qDebug() << m_result;
    return !m_result.isEmpty();
}

QVariant SvgDataResolver::formatData(const QString &svgFieldName, DataModel *dataModel)const
{
    DataModelScriptable dm(m_drScriptEngine, dataModel);
    QScriptValue drScriptMain = m_drScriptEngine->evaluate(RESLV_FUNC_FORMAT_DATA);
    if(!drScriptMain.isFunction())
    {
//        qDebug() << "ERROR: Can't evaluate function" << QObject::tr(RESLV_FUNC_FORMAT_DATA);
        return QVariant();
    }
    QScriptValue drScriptFormat = drScriptMain.call(QScriptValue(), QScriptValueList()<<svgFieldName
                                                    << m_drScriptEngine->newObject(&dm)
                                                    );

    if(drScriptFormat.isUndefined())
        return QVariant();
    else
        return drScriptFormat.toVariant();
}

