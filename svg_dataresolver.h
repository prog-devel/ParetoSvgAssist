/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: svg_dataresolver.h
** Created: 2011-04-29 00:43:26.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#ifndef SVG_DATARESOLVER_H
#define SVG_DATARESOLVER_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#define RESLV_ID_ATTR "field"
#define RESLV_FUNC_FORMAT_DATA "svgFormatData"

class DataModel;

class SvgDataResolver
{
public:
    SvgDataResolver(const QString &input, QScriptEngine *drScriptEngine);

    bool resolve(DataModel *dataModel);
    inline QString result()const{return m_result;}
    void clear(){m_result.clear();}

    inline bool isValid()const{return !m_input.isEmpty();}
    inline bool isResolved()const{return !m_result.isEmpty();}

    inline QString inputSvgRawData()const{return m_input;}


protected:
    QVariant formatData(const QString &svgFieldName, DataModel *model)const;

private:
    QString m_result;
    QString m_input;
    QScriptEngine *m_drScriptEngine; //data-resolver script-engine
};


#endif // SVG_DATARESOLVER_H
