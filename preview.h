/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: preview.h
** Created: 2011-04-30 16:11:12.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#ifndef PREVIEW_H
#define PREVIEW_H

#include <QtGui>

//#define USE_CACHE

#define SVG_MARGINS 45
#define PAPER_MARGINS 10
#define ZOOM_STEP 0.1
#define WHEEL_STEP 0.1
#define MAX_ZOOM 5.0
#define MIN_ZOOM 0.2

class QSvgRenderer;

class SvgWidget: public QWidget
{
    Q_OBJECT

public:
    SvgWidget(QWidget *parent=0);
    inline QSvgRenderer *renderer()const{return m_renderer;}

    inline bool zoomInEnabled()const{return m_scaleFactor < MAX_ZOOM;}
    inline bool zoomOutEnabled()const{return m_scaleFactor > MIN_ZOOM;}
    inline bool zoomResetEnabled()const{return m_scaleFactor != 1.0;}

    inline qreal zoomFactor()const{return m_scaleFactor;}

signals:
    void zoomInEnableChanged(bool);
    void zoomOutEnableChanged(bool);
    void zoomResetEnableChanged(bool);
    void zoomFactorChanged(qreal);

public slots:
    void zoomIn();
    void zoomOut();
    void zoomReset();
    void scale(qreal scaleFactor);

protected:
    void paintEvent(QPaintEvent *);
    void wheelEvent(QWheelEvent *);
    QSize sizeHint()const;

    qreal svgMargins()const;
    QSize svgSize()const;

protected slots:
    void slotRepaintNeeded();

private:
    QSvgRenderer *m_renderer;
    qreal m_scaleFactor;
#ifdef USE_CACHE
    QImage m_cache;
#endif
};

class Preview: public QScrollArea
{
    Q_OBJECT

public:
    Preview(QWidget *parent=0);

    QSvgRenderer *renderer()const;

    inline qreal zoomFactor()const{return dynamic_cast<SvgWidget *>(widget())->zoomFactor();}

signals:
    void zoomFactorChanged(qreal);

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

private:
    QPoint m_basePos;
};

class PreviewDialog: public QDialog
{
    Q_OBJECT
public:
    PreviewDialog(QWidget *parent=0);

    inline QSvgRenderer *renderer()const{return m_pw->renderer();}
    inline QString fileName()const{return m_fileName;}

protected slots:
    void slotSaveQuery();
    void slotCancelQuery();
    void updateZoomTip();

private:
    Preview *m_pw;
    QString m_fileName;
    QSpinBox *m_spZoom;
};

#endif // PREVIEW_H
