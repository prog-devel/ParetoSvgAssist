######################################################################################
## Copyright (C) 2013 ProgDevel. All rights reserved.
##
## Author: Kirill Geyserov (aka ProgDevel)
## Contact: prog.devel@gmail.com
##
## File: svgassist.pro
## Created: 2013-08-19 03:15:58.000000000 +0400
##
## LICENSE NOTICE:
##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License (GPLv3+) as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
######################################################################################

VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_PATCH = 0

DEFINES += "VERSION_MAJOR=$$VERSION_MAJOR" \
       "VERSION_MINOR=$$VERSION_MINOR" \
       "VERSION_PATCH=$$VERSION_PATCH"

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_PATCH}

QT       += core xml script svg
QT       += gui

TARGET = svgassist
TEMPLATE = app

DESTDIR		= .bin
MOC_DIR		= .gen/moc
UI_DIR		= .gen/ui
OBJECTS_DIR	= .gen/obj
RCC_DIR		= .gen/rcc

SOURCES += main.cpp\
    svg_dataresolver.cpp \
    svg_datamodel.cpp \
    preview.cpp

HEADERS  += \
    svg_dataresolver.h \
    svg_datamodel.h \
    preview.h

OTHER_FILES += \
    data_resolver.js \
    style.css \
    TODO.txt

RESOURCES += \
    resources.qrc

CODECFORTR = UTF-8
TRANSLATIONS += locale/trans_ru_RU.ts 
