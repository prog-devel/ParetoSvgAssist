/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: svg_datamodel.cpp
** Created: 2011-04-29 03:25:49.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#include "svg_datamodel.h"

#include <QDebug>

DataModel::DataModel()
{

}

bool DataModel::fromXml(const QString &fileName)
{
    clear();
    QFile inFile(fileName);
    if(!inFile.open(QFile::ReadOnly))
    {
        m_reader.raiseError(QObject::tr("File open error: %1").arg(fileName));
        return false;
    }
    m_reader.setDevice(&inFile);
    if(m_reader.readNextStartElement() && !m_reader.hasError())
    {
        if(m_reader.qualifiedName()!=DM_NS":data" || m_reader.attributes().value("version")!=DM_VER)
        {
            m_reader.raiseError(QObject::tr("The file is not valid PPD version %1 file").arg(DM_VER));
        }
        else
        {
            m_templateFileName = m_reader.attributes().value("template").toString();
            return readXmlData();
        }

    }
    return !m_reader.hasError();
}

bool DataModel::readXmlData()
{
    while(m_reader.readNextStartElement() && !m_reader.hasError())
    {
        //проверка пространств имен происходит в самом парсере
        QString fieldName = m_reader.name().toString();
        if(m_data.contains(fieldName))
        {
            m_reader.raiseError(QObject::tr("Redefinition of field %1").arg(fieldName));
        }
        else
        {
            //аттрибут value приоритетней, чем текст
            QString text = m_reader.attributes().value("value").toString();
            //но если он не установлен, то подсасываем текст
            if(text.isEmpty())
                text = m_reader.readElementText();
            m_data[fieldName] = text;

            if(!m_reader.isEndElement())
                m_reader.skipCurrentElement();
        }
    }
    return !m_reader.hasError();
}

QVariant DataModel::data(const QString &fieldName)const
{
    QHash<QString, QVariant>::const_iterator it = m_data.find(fieldName);
    return it!=m_data.end() ? *it : QVariant();
}

void DataModel::clear()
{
    m_templateFileName.clear();
    m_data.clear();
    m_reader.clear();
}

QString DataModel::errorString()const
{
    return QObject::tr("[DataModel]: %1\nLine %2, column %3")
            .arg(m_reader.errorString())
            .arg(m_reader.lineNumber())
            .arg(m_reader.columnNumber());
}

//====================================================================================

QScriptValue DataModelScriptable::data(QScriptContext *ctx, QScriptEngine * eng)
{
    QScriptValue dataFieldName = ctx->argument(0);
    DataModelScriptable *dm = dynamic_cast<DataModelScriptable *>(ctx->thisObject().scriptClass());
    QVariant value = dm->m_model->data(dataFieldName.toString());
    return dm && value.isValid() ? QScriptValue(value.toString()) : eng->undefinedValue();
}

DataModelScriptable::DataModelScriptable(QScriptEngine *e, DataModel *model): QScriptClass(e), m_model(model)
{
    m_dataFunc = e->newFunction(data, 1);
    m_propData = e->toStringHandle(DM_SCRIPTABLE_DATA_FUNC);
}

DataModelScriptable::QueryFlags DataModelScriptable::queryProperty(const QScriptValue & /*object*/, const QScriptString &name, QueryFlags flags, uint * /*id*/)
{
    if(name==m_propData)
        return flags & HandlesReadAccess;
    else
        return 0;
}

QScriptValue DataModelScriptable::property(const QScriptValue & /*object*/, const QScriptString &name, uint /*id*/)
{
    if(name==m_propData)
        return m_dataFunc;
    else
        return QScriptValue();
}

