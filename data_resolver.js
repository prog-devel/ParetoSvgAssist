function svgFormatData(svgField, dataModel)
{
    //прямое отображение имени текстового поля в название поля модели
    //но можно ухищриться и что-нибудь эдакое аж с вычислениями забубенить
    var data = dataModel.data(svgField);
    if(data === undefined)
        return undefined;
    if(svgField == "order_no")
        return "Заказ №" + data;
    else
        return data + " мм";
}
