/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: svg_datamodel.h
** Created: 2011-04-29 03:23:02.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#ifndef SVG_DATAMODEL_H
#define SVG_DATAMODEL_H

#include <QtXml>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>
#include <QtScript/QScriptClass>
//#include <QtScript/QScriptable>
#include <QtScript/QScriptString>

#define DM_NS "ppd" //Namespace-prefix of data-model
#define DM_VER "1" //Version of data-model
#define DM_SCRIPTABLE_DATA_FUNC "data"

class DataModel
{
public:
    DataModel();
    bool fromXml(const QString &fileName);
    QVariant data(const QString &fieldName)const;
    void clear();

    QString errorString()const;

    inline QString templateFileName()const{return m_templateFileName;}

protected:
    bool readXmlData();

private:
    QHash<QString, QVariant> m_data;
    QString m_templateFileName;
    QXmlStreamReader m_reader;
};

//враппер над моделью данных, реализующий запросы к данным из скрипта
class DataModelScriptable: public QScriptClass
{
public:
    DataModelScriptable(QScriptEngine *e, DataModel *model);
    QueryFlags queryProperty(const QScriptValue & /*object*/, const QScriptString &name, QueryFlags flags, uint * /*id*/);
    QScriptValue property(const QScriptValue & /*object*/, const QScriptString &name, uint /*id*/);

private:
    static QScriptValue data(QScriptContext *ctx, QScriptEngine *eng);

    QScriptString m_propData;
    QScriptValue m_dataFunc;
    DataModel *m_model;
};

#endif // SVG_DATAMODEL_H
