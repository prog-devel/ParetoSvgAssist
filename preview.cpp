/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: preview.cpp
** Created: 2011-04-30 16:20:11.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#include "preview.h"

#include <QtSvg/QSvgRenderer>
#include <QtSvg/QSvgWidget>

SvgWidget::SvgWidget(QWidget *parent)
    :QWidget(parent)
{
    m_scaleFactor = 1.0;
    m_renderer = new QSvgRenderer(this);
    connect(m_renderer, SIGNAL(repaintNeeded()), this, SLOT(slotRepaintNeeded()));

    //setBackgroundRole(QPalette::Light);
    //setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void SvgWidget::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    p.setClipRect(e->rect());
    QPoint paperPos(PAPER_MARGINS, PAPER_MARGINS);
    QRect pr;
    pr.translate(paperPos);
    pr.setSize(size() - QSize(PAPER_MARGINS, PAPER_MARGINS)*2);
    p.setBrush(QColor(255,255,255));
    p.setPen(Qt::SolidLine);
    p.drawRect(pr);

#ifndef USE_CACHE
    QRect br;
    qreal sm = svgMargins();
    br.translate(paperPos + QPoint(sm, sm));
    br.setSize(svgSize());
    m_renderer->render(&p, br);
#else
    if(!m_cache.isNull())
        p.drawImage(0, 0, m_cache);
#endif
}

QSize SvgWidget::sizeHint()const
{
    return QWidget::sizeHint();
}

void SvgWidget::slotRepaintNeeded()
{
    qreal sm = svgMargins();
    QSize paperSize = svgSize() + QSize(sm, sm)*2;

#ifdef USE_CACHE
    QPoint paperPos(PAPER_MARGINS, PAPER_MARGINS);

    m_cache = QImage(paperSize, QImage::Format_ARGB32);
    m_cache.fill(0);
    QPainter p(&m_cache);
    QRect br;
    br.translate(paperPos + QPoint(sm, sm));
    br.setSize(svgSize());
    m_renderer->render(&p, br);
    p.end();
#endif

    setFixedSize(paperSize + QSize(PAPER_MARGINS, PAPER_MARGINS)*2);
}

qreal SvgWidget::svgMargins()const
{
    return SVG_MARGINS*m_scaleFactor;
}

QSize SvgWidget::svgSize()const
{
    return m_renderer->defaultSize()*m_scaleFactor;
}

void SvgWidget::scale(qreal scaleFactor)
{
    if(
            (scaleFactor > m_scaleFactor && m_scaleFactor>=MAX_ZOOM)
            || (scaleFactor < m_scaleFactor && m_scaleFactor<=MIN_ZOOM)
            || m_scaleFactor == scaleFactor
            )
        return;

    bool zie = zoomInEnabled();
    bool zoe = zoomOutEnabled();
    bool zre = zoomResetEnabled();

    m_scaleFactor = scaleFactor;
    if(qAbs(m_scaleFactor-1.0) < 0.00001)
        m_scaleFactor = 1.0;
    slotRepaintNeeded();

    bool nzie = zoomInEnabled();
    bool nzoe = zoomOutEnabled();
    bool nzre = zoomResetEnabled();

    if(nzie!=zie)
        emit zoomInEnableChanged(nzie);
    if(nzoe!=zoe)
        emit zoomOutEnableChanged(nzoe);
    if(nzre!=zre)
        emit zoomResetEnableChanged(nzre);

    emit zoomFactorChanged(m_scaleFactor);
}

void SvgWidget::zoomIn()
{
    scale(m_scaleFactor * (1+ZOOM_STEP));
}

void SvgWidget::zoomOut()
{
    scale(m_scaleFactor / (1+ZOOM_STEP));
}

void SvgWidget::zoomReset()
{
    scale(1.0);
}

void SvgWidget::wheelEvent(QWheelEvent *e)
{
    if(e->modifiers() == Qt::ControlModifier && e->orientation()==Qt::Vertical)
    {
        qreal delta = qreal(qAbs(e->delta()))/8.0/15.0;
        delta = qPow((1+WHEEL_STEP), delta);
        if(e->delta()>0)
            scale(m_scaleFactor * delta);
        else
            scale(m_scaleFactor / delta);
        slotRepaintNeeded();
    }
    else
        QWidget::wheelEvent(e);
}

//====================================================

Preview::Preview(QWidget *parent)
    :QScrollArea(parent)
{
    setAlignment(Qt::AlignCenter);
    setBackgroundRole(QPalette::Dark);
    //setWidgetResizable(true);
    SvgWidget *svgWidget = new SvgWidget;
    setWidget(svgWidget);

    QAction *actZoom = new QAction(QIcon(":/images/zoom_in"), tr("Zoom in"), this);
    actZoom->setShortcut(QKeySequence(tr("Ctrl++")));
    connect(actZoom, SIGNAL(triggered()), svgWidget, SLOT(zoomIn()));
    connect(svgWidget, SIGNAL(zoomInEnableChanged(bool)), actZoom, SLOT(setEnabled(bool)));
    actZoom->setEnabled(svgWidget->zoomInEnabled());
    addAction(actZoom);

    actZoom = new QAction(QIcon(":/images/zoom_out"), tr("Zoom out"), this);
    actZoom->setShortcut(QKeySequence(tr("Ctrl+-")));
    connect(actZoom, SIGNAL(triggered()), svgWidget, SLOT(zoomOut()));
    connect(svgWidget, SIGNAL(zoomOutEnableChanged(bool)), actZoom, SLOT(setEnabled(bool)));
    actZoom->setEnabled(svgWidget->zoomOutEnabled());
    addAction(actZoom);

    actZoom = new QAction(QIcon(":/images/zoom_reset"), tr("Zoom reset"), this);
    actZoom->setShortcut(QKeySequence(tr("Ctrl+0")));
    connect(actZoom, SIGNAL(triggered()), svgWidget, SLOT(zoomReset()));
    connect(svgWidget, SIGNAL(zoomResetEnableChanged(bool)), actZoom, SLOT(setEnabled(bool)));
    actZoom->setEnabled(svgWidget->zoomResetEnabled());
    addAction(actZoom);

    connect(svgWidget, SIGNAL(zoomFactorChanged(qreal)), this, SIGNAL(zoomFactorChanged(qreal)));
}

QSvgRenderer *Preview::renderer()const
{
    return dynamic_cast<SvgWidget *>(widget())->renderer();
}

void Preview::mousePressEvent(QMouseEvent *e)
{
    QScrollArea::mousePressEvent(e);
}

void Preview::mouseMoveEvent(QMouseEvent *e)
{
    QScrollArea::mouseMoveEvent(e);
}

void Preview::mouseReleaseEvent(QMouseEvent *e)
{
    QScrollArea::mouseReleaseEvent(e);
}

//=======================================================

PreviewDialog::PreviewDialog(QWidget *parent)
    :QDialog(parent)
{
    QVBoxLayout *ml = new QVBoxLayout(this);
    QHBoxLayout *hl = new QHBoxLayout();
    m_pw = new Preview(this);
    QToolBar *tb = new QToolBar(this);
    hl->addWidget(tb);

    m_spZoom = new QSpinBox(this);
    m_spZoom->setReadOnly(true);
    m_spZoom->setButtonSymbols(QSpinBox::NoButtons);
    m_spZoom->setSuffix(tr("%"));
    m_spZoom->setRange(0, 10000);
    tb->addWidget(new QLabel(tr("Zoom:")));
    tb->addWidget(m_spZoom);
    tb->addActions(m_pw->actions());

    ml->addLayout(hl);
    ml->addWidget(m_pw);

    QDialogButtonBox *bb = new QDialogButtonBox(this);
    hl->addWidget(bb);

    bb->addButton(QDialogButtonBox::Save);
    bb->addButton(QDialogButtonBox::Cancel);

    connect(bb, SIGNAL(accepted()), this, SLOT(slotSaveQuery()));
    connect(bb, SIGNAL(rejected()), this, SLOT(slotCancelQuery()));

    updateZoomTip();
    connect(m_pw, SIGNAL(zoomFactorChanged(qreal)), this, SLOT(updateZoomTip()));
}

void PreviewDialog::updateZoomTip()
{
    m_spZoom->setValue(m_pw->zoomFactor()*100);
}

void PreviewDialog::slotSaveQuery()
{
    m_fileName = QFileDialog::getSaveFileName(0, QObject::tr("Save in PDF format"), QString(), "Portable document (*.pdf)");
    if(m_fileName.isEmpty())
        return;
    if(!m_fileName.endsWith(".pdf", Qt::CaseInsensitive))
        m_fileName += ".pdf";
    accept();
}

void PreviewDialog::slotCancelQuery()
{
    m_fileName.clear();
    reject();
}
