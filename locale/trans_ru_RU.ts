<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name></name>
    <message>
        <source>Svg Assist</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Preview</name>
    <message>
        <source>Zoom in</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation></translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <source>Zoom reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PreviewDialog</name>
    <message>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <source>Zoom:</source>
        <translation>Масштаб:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>: Warning</source>
        <translation>: Предупреждение</translation>
    </message>
    <message>
        <source>: Critical</source>
        <translation>: Ошибка</translation>
    </message>
    <message>
        <source>: Fatal</source>
        <translation>: Сбой</translation>
    </message>
    <message>
        <source>Invalid path to data-file</source>
        <translation>Неверный путь к исходным данным</translation>
    </message>
    <message>
        <source>Unable to load script</source>
        <translation>Не могу загрузить скрипт</translation>
    </message>
    <message>
        <source>Error in</source>
        <translation>Ошибка в</translation>
    </message>
    <message>
        <source>at line</source>
        <translation>в строке</translation>
    </message>
    <message>
        <source>File open error: %1</source>
        <translation>Ошибка открытия файла: %1</translation>
    </message>
    <message>
        <source>The file is not valid PPD version %1 file</source>
        <translation>Некорректный файл данных (PPD версии %1)</translation>
    </message>
    <message>
        <source>Redefinition of field %1</source>
        <translation>Переопределение поля данных %1</translation>
    </message>
    <message>
        <source>[DataModel]: %1
Line %2, column %3</source>
        <translation>[Данные]: %1 Строка %2, Позиция %3</translation>
    </message>
    <message>
        <source>Save in PDF format</source>
        <translation>Сохраненить как PDF</translation>
    </message>
</context>
</TS>
