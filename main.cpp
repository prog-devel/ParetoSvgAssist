/*************************************************************************************
** Copyright (C) 2013 ProgDevel. All rights reserved.
**
** Author: Kirill Geyserov (aka ProgDevel)
** Contact: prog.devel@gmail.com
**
** File: main.cpp
** Created: 2013-08-19 02:54:19.000000000 +0400
**
** LICENSE NOTICE:
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License (GPLv3+) as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*************************************************************************************/

#include <QtSvg/QSvgRenderer>
#include <QtGui>
#include <QDebug>

#include <cstdio>
#include <cstdlib>

#include "preview.h"
#include "svg_dataresolver.h"
#include "svg_datamodel.h"

#define APP_NAME QT_TR_NOOP("Svg Assist")
#define TRANSLATIONS_DIR "locale"

void logHandler(QtMsgType type, const char *msg)
{
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", msg);
        break;
    case QtWarningMsg:
        //fprintf(stderr, "Warning: %s\n", msg);
        QMessageBox::warning(0, qApp->applicationName()+QObject::tr(": Warning"), QString::fromLocal8Bit(msg));
        break;
    case QtCriticalMsg:
        //fprintf(stderr, "Critical: %s\n", msg);
        QMessageBox::critical(0, qApp->applicationName()+QObject::tr(": Critical"), QString::fromLocal8Bit(msg));
        break;
    case QtFatalMsg:
        //          fprintf(stderr, "Fatal: %s\n", msg);
        QMessageBox::critical(0, qApp->applicationName()+QObject::tr(": Fatal"), QString::fromLocal8Bit(msg));
        abort();
    }
}

int main(int argc, char *argv[])
{
    qInstallMsgHandler(logHandler);
    QApplication a(argc, argv);
    a.setApplicationName(QObject::tr(APP_NAME));
    a.setApplicationVersion(QString("%1.%2").arg(VERSION_MAJOR).arg(VERSION_MINOR));

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    //QLocale::setDefault(QLocale("ru_RU"));
    QTranslator appTrans;
    QTranslator qtTrans;

    const QString localeSuffix = QLocale::system().name();
    const QString transDir = qApp->applicationDirPath() + "/"TRANSLATIONS_DIR;
    qDebug() << "Locale:" << localeSuffix;

    // загружаем перевод для программы
    if(!appTrans.load("trans_"+localeSuffix,  transDir) || !qtTrans.load("qt_"+localeSuffix, transDir))
        qWarning() << "Can't setup app-translations for lang:" << localeSuffix;
    else
        qApp->installTranslator(&appTrans);

    //загружаем перевод для Qt-библиотечных компонентов
    if(!qtTrans.load("qt_"+localeSuffix, transDir))
        qWarning() << "Can't setup QT-translations for lang:" << localeSuffix;
    else
        qApp->installTranslator(&qtTrans);

    if(argc<2)
    {
        qCritical() << QObject::tr("Invalid path to data-file");
        return -1;
    }
    QString dataFileName = QString::fromLocal8Bit(argv[1]);

    QScriptEngine drScriptEngine;
    QString drScriptFileName = "data_resolver.js";
    QFile drScriptFile(drScriptFileName);
    if(!drScriptFile.open(QIODevice::ReadOnly))
    {
        qCritical() << QObject::tr("Unable to load script") << drScriptFileName;
        return -1;
    }
    drScriptEngine.evaluate(drScriptFile.readAll(), drScriptFileName);
    if(drScriptEngine.hasUncaughtException())
    {
        qCritical() << QObject::tr("Error in") << drScriptFileName << ":" << drScriptEngine.uncaughtException().toString()
                 << QObject::tr("at line") << drScriptEngine.uncaughtExceptionLineNumber()
                    ;
        return -1;
    }
    drScriptFile.close();

    DataModel model;
    if(!model.fromXml(dataFileName))
    {
        qCritical() << model.errorString();
        return -1;
    }

    if(model.templateFileName().isEmpty())
    {
        qCritical() << "Invalid path to template";
        return -1;
    }

    QString svgStr;
    QFile svgFile(model.templateFileName());
    QTextStream svgStream(&svgFile);
    svgStream.setCodec(QTextCodec::codecForName("UTF8"));
    if(!svgFile.open(QFile::ReadOnly) || (svgStr = svgStream.readAll() , svgFile.error()!=QFile::NoError))
    {
        qCritical() << "Svg file open/read error" << svgFile.errorString();
        return -1;
    }
    SvgDataResolver resl(svgStr, &drScriptEngine);
    if(!resl.resolve(&model))
    {
        return -1;
    }
    QString svgData = resl.result();

    PreviewDialog w;
    w.setWindowTitle(qApp->applicationName() + " Ver: " + qApp->applicationVersion());
    QSvgRenderer &rend = *w.renderer();
    QXmlStreamReader svgReader(svgData);
    if(!rend.load(&svgReader))
    {
        qCritical() << "Invalid svg data-format: " << (svgReader.hasError() ? svgReader.errorString() : "Unknown error");
        return -1;
    }

    if(w.exec() == QDialog::Accepted)
    {
        QString fileName = w.fileName();
        if(!fileName.endsWith(".pdf", Qt::CaseInsensitive))
            fileName += ".pdf";

        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);

        QPainter painter;
        if(!painter.begin(&printer))
        {
            qCritical() << "Can't create PDF output";
            return -1;
        }
        rend.render(&painter);
        painter.end();
    }

    return 0;
}
